
import java.util.Scanner;

class Addition {

	static Scanner sc = new Scanner(System.in); 
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
			double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
		
		//Programmhinweis
			System.out.println("Hinweis: ");
			System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
		
		//4. Eingabe
			System.out.println("1. Zahl:");
			zahl1 = sc.nextDouble();
	        System.out.println(" 2. Zahl:");
	        zahl2 = sc.nextDouble();

	    //3.Verarbeitung
	        erg = zahl1 + zahl2;

	    //2.Ausgabe
	        System.out.println("Ergebnis der Addition");
	        System.out.printf("%.2f = %.2f+%.2f", erg, zahl1, zahl2);
		
		
		
	}

}
