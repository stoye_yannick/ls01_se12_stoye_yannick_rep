﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static double fahrkartenbestellungErfassen(){
		double zuZahlenderBetrag;
		int anzahlFahrkarten;
		Scanner tastatur = new Scanner(System.in);

		System.out.print("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();
		System.out.println("Wie viele Fahrscheine wollen Sie kaufen? ");
		anzahlFahrkarten = tastatur.nextInt();
		if (anzahlFahrkarten < 0) {
			System.out.println("Bitte geben Sie eine positive Zahl ein.");
			anzahlFahrkarten = 1;
		} else if (anzahlFahrkarten < 1 || anzahlFahrkarten > 10) {
			System.out.println("Bitte geben Sie nur Zahlen zwischen 1 und 10 ein.");
			anzahlFahrkarten = 1;
		}
		zuZahlenderBetrag = zuZahlenderBetrag * anzahlFahrkarten;
		return zuZahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double rückgabebetrag;
		Scanner tastatur = new Scanner(System.in);
		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			// System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag -
			// eingezahlterGesamtbetrag));
			double zwischenergebnis = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f Euro\n", zwischenergebnis);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();

		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return rückgabebetrag;
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.00) {
			System.out.println("Der Rückgabebetrag in H�he von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rückgabebetrag >= 2.00) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.00;
			}
			while (rückgabebetrag >= 1.00) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.00;
			}
			while (rückgabebetrag >= 0.50) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.50;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.20;
			}
			while (rückgabebetrag >= 0.10) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.10;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

	public static void main(String[] args) 
	{

		double zuZahlenderBetrag;
		double rückgabebetrag;

		while (true) 
		{
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			rueckgeldAusgeben(rückgabebetrag);
			System.out.println("\n");
		}

	}
}
